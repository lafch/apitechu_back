const global     = require('../global.js');
var requestJson  = require('request-json');
var client       = requestJson.createClient(global.URL_BASE_MONGO);

function getAccountByIdUserMongo(req,res){
  var idUser = req.params.id;
  var queryStringID = "";
  if(idUser){
    queryStringID = 'q={"id_user":'+idUser+'}&';
  }
  var queryString = 'f={"_id": 0}&';
  client.get('account?' + queryStringID + queryString + global.API_KEY,
    function(err, respuestaMLab, body) {
      var response = {};
      if(err) {
          response = {"msg" : "Error obteniendo cuenta."}
          res.status(500);
      } else {
        if(body.length > 0) {
          console.log(body.length);
          response = body;
        } else {
          response = {"msg" : "Ningún elemento 'account'."}
          res.status(404);
        }
      }
      res.send(response);
 });

}
module.exports.getAccountByIdUserMongo = getAccountByIdUserMongo;
