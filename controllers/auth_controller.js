var user_file = require('../data/user.json');
//LOGIN IN -file-system
function postLogin(req,res){
  let email = req.body.email;
  let pass = req.body.password;
  let tam = user_file.length;
  let i = 0;
  let encontrado = false;
  while( i< user_file.length && !encontrado){
    if(user_file[i].email == email && user_file[i].password == pass){
      encontrado = true;
      user_file[i].logged = encontrado;
    }
    i++;
  }
  if(encontrado){
    writeUserDataToFile(user_file);
    res.send({ "mensaje" : "login correcto", "idUsuario" : i });
  }else{
    res.send({ "mensaje" : "login incorrecto"});
  }
}

// LOGIN OUT file-system
function postLoginOut(req,res){
  user_file.forEach(function(user){
    if(user.email == req.body.email && user.password == req.body.password){
      if(user.logged){
        res.status(200);
        delete user.logged;
        writeUserDataToFile(user_file);
        res.send({ "mensaje" : "logout correcto", "idUsuario" : user.ID });
      }else{
        res.status(404)
        res.send({ "mensaje" : "usuario no se encuentra logeado" });
      }

    }
  });
  res.status(404)
  res.send({ "mensaje" : "logout incorrecto" });
}

module.exports = {
  postLogin,
  postLoginOut
};

function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json",jsonUserData,"utf8",
  function(err){ // funcion manejadora para gestionar errores de escritura
    if(err){
      console.log(err);
    }else{
      console.log("Datos escritos en 'users.json'");
    }
  });
}

//ANTIGUO - REFERENCIA
// POST login
// httpClient.post(global.URL_BASE + 'login',
//   function(req,res){
//     user_file.forEach(function(user){
//       if(user.email == req.body.email && user.password == req.body.password){
//         res.status(200);
//         if(user.logged){
//           user.logged = true;
//           res.send({ "mensaje" : "login correcto", "idUsuario" : user.ID });
//         }else{
//           res.send({ "mensaje" : "ya se encuentra logeado", "idUsuario" : user.ID });
//         }
//       }
//     });
//     res.status(404)
//     res.send({"msg":"Usuario no existe"});
//
//   }
// );

// POST login-Out
// httpClient.post(global.URL_BASE + 'loginOut',
//   function(req,res){
//     user_file.forEach(function(user){
//       if(user.email == req.body.email && user.password == req.body.password){
//         if(user.logged){
//           res.status(200);
//           delete user.logged;
//           res.send({ "mensaje" : "logout correcto", "idUsuario" : user.ID });
//         }else{
//           res.status(404)
//           res.send({ "mensaje" : "usuario no se encuentra logeado" });
//         }
//
//       }
//     });
//     res.status(404)
//     res.send({ "mensaje" : "logout incorrecto" });
//
//   }
// );
//
// httpClient.get(global.URL_BASE + 'userslogout',
//   function(req,res){
//     let logoutUser = [];
//     let cantidadMostrar = req.query.cantidadRegistro != null ? req.query.cantidadRegistro : user_file.length ;
//     console.log("cantidad Mostrar: " + cantidadMostrar);
//     let i = 0;
//     let cantidadLogeados = 0;
//
//     user_file.forEach(function(user){
//         if(user.logged){
//           if(i<cantidadMostrar){
//             logoutUser.push(user);
//             i++;
//           }
//           cantidadLogeados++;
//         }
//     });
//
//     if(logoutUser.length > 0){
//       let cantidad = {
//         "cantidadRequerida": cantidadMostrar,
//         "cantidadLogeados":cantidadLogeados
//       }
//       logoutUser.push(cantidad);
//       res.status(200);
//       res.send(logoutUser);
//     }
//     res.status(404)
//     res.send({ "mensaje" : "ningun usuario con estado logged" });
// });

var requestJson  = require('request-json');
var client       = requestJson.createClient(process.env.URL_BASE_MONGO);
var apiKey = process.env.MLAB_API_KEY;

function postLoginMongo(req,res){
  let email = req.body.email;
  let pass = req.body.password;
  var queryString = `q={"email": "${email}", "password": "${pass}"}&`;
  let limFilter = "l=1&";
  console.log(queryString);
  client.get('user?'+queryString+limFilter+apiKey,
    function(err, respuestaMLab, body){
      if(err){
        res.status(500);
        res.send(err);
      }else{
        if(body.length == 1){
          let login = '{"$set":{"logged":true}}';
            client.put('user/' + body[0]._id.$oid + '?' + apiKey, JSON.parse(login),
              function(errPut, resPut, bodyPut) {
                if(errPut){
                  res.sebd(errPut);
                }else{
                  res.send(bodyPut);
                }
              });
        }else{
           res.status(404);
           res.send({"msg":"Usuario no existe"});
        }
      }
    }
  );
}
module.exports.postLoginMongo = postLoginMongo;

function postLoginShortMongo(req,res){
  let email = req.body.email;
  let pass = req.body.password;
  var queryString = `q={"email": "${email}", "password": "${pass}"}&`;
  let limFilter = "l=1&";
  let login = '{"$set":{"logged":true}}';
  client.put('user?'+queryString+apiKey, JSON.parse(login),
      function(errPut, resPut, bodyPut) {
        if(errPut){
          res.send(errPut);
        }else{
          if(bodyPut.n==1){
            res.send({"msg":"Usuario logeado exitosamente"});
          }else{
            res.send({"msg":"Usuario no existe"});
          }
        }
  });
}
module.exports.postLoginShortMongo = postLoginShortMongo;

function postLoginOutMongo(req,res){
  let email = req.body.email;
  let pass = req.body.password;
  var queryString = `q={"email": "${email}", "password": "${pass}"}&`;
  let limFilter = "l=1&";
  var putBody = '{"$unset":{"logged":""}}'
  client.put('user?'+queryString+apiKey, JSON.parse(putBody),
      function(errPut, resPut, bodyPut) {
        if(errPut){
          res.send(errPut);
        }else{
          if(bodyPut.n==1){
            res.send({"msg":"Usuario deslogeado exitosamente"});
          }else{
            res.send({"msg":"Usuario no existe"});
          }
        }
  });
}
module.exports.postLoginOutMongo = postLoginOutMongo;
