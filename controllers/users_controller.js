const global     = require('../global.js');
var user_file    = require('../data/user.json');
var requestJson  = require('request-json');
var client       = requestJson.createClient(global.URL_BASE_MONGO);

// GET with parameter
function getSearchUserParameter(req,res) {
  let pos = req.params.id-1;
  let respuesta = (user_file[pos] == undefined) ? {"msg":"usuario no encontrado"} : user_file[pos];
  res.send(respuesta);
}

module.exports.getSearchUserParameter = getSearchUserParameter;

function getSearchUserQueryString(req,res) {
  console.log('GET with QueryString');
  console.log(req.query.id);
  console.log(req.query.first_name);

    let i= 0;
    let encontrado = false;
    let user = null;
    while(i<user_file.length && !encontrado){
      if(req.query.id && req.query.first_name){
        if(user_file[i].ID == req.query.id && user_file[i].first_name == req.query.first_name){
          user = user_file[i];
          encontrado = true;
        }
      }else if(req.query.id){
        if(user_file[i].ID == req.query.id){
          user = user_file[i];
          encontrado = true;
        }
      }else if(req.query.first_name){
        if(user_file[i].first_name == req.query.first_name){
          user = user_file[i];
          encontrado = true;
        }
      }
      i++;
    }

    if(user){
      res.status(200);
      res.send(user);
    }else{
      if(!req.query.first_name && !req.query.id){
        res.status(200);
        res.send(user_file);
      }else{
        res.status(403);
        res.send({"msg":"Usuario no existe"})
      }

    }
}

module.exports.getSearchUserQueryString = getSearchUserQueryString;

// POST add User
function postAddUser(req,res){
  let newId = user_file.length+1;
  let newUser = {
    "id": newId,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": req.body.password
  };
  user_file.push(newUser);
  res.send(newUser);
}
module.exports.postAddUser = postAddUser;

function putModifyUser(req,res) {
  let pos = req.params.id-1;
  if(user_file[pos]){
    let modifyUser = new Object();
    if(req.body.first_name){
      user_file[pos].first_name = req.body.first_name;
    }
    if(req.body.last_name){
      user_file[pos].last_name = req.body.last_name;
    }
    if(req.body.email){
      user_file[pos].email = req.body.email;
    }
    if(req.body.password){
      user_file[pos].first_name = req.body.password;
    }
    res.send(user_file[pos]);
  }
  res.send('no existe');
}
module.exports.putModifyUser = putModifyUser;

function deleteUser() {
  let pos = req.params.id-1;
  if(user_file[pos]){
    user_file.splice(pos,1);
    res.status(200);
    res.send({"msg":"Usuario eliminado"});
  }
  res.status(404)
  res.send({"msg":"Usuario no existe"});
}
module.exports.deleteUser = deleteUser;

//const apikeyMlab = (JSON.parse(process.env.MLAB_API_KEY)).value;
const apikeyMlab = process.env.MLAB_API_KEY;
//MONGO DB
function getUsersMongo(req,res){
  console.log(apikeyMlab);
  var idUser = req.params.id;
  var queryStrField= 'f={"_id":0}&';
   client.get('user?' + queryStrField + apikeyMlab,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
  });
}
module.exports.getUsersMongo = getUsersMongo;

function getUserByIdMongo(req,res){
  var idUser = req.params.id;
  var queryString;
  if(idUser){
    queryString = 'q={"ID":'+idUser+'}&';
  }
  var queryStrField= 'f={"_id":0}&';
   client.get('user?' + queryString + queryStrField + global.API_KEY,
     function(err, respuestaMLab, body) {
       var response = {};
       if(err) {
           response = {"msg" : "Error obteniendo usuario."}
           res.status(500);
       } else {
         if(body.length > 0) {
           console.log(body.length);
           response = body;
         } else {
           response = {"msg" : "Ningún elemento 'user'."}
           res.status(404);
         }
       }
       res.send(response);
  });
}
module.exports.getUserByIdMongo = getUserByIdMongo;

function postCreateUserMongo(req,res){
  client.get('user?' + global.API_KEY,
  function(err, respuestaMLab, body) {
    var response = {};
    if(err) {
        response = {"msg" : "Error obteniendo usuario."}
        res.status(500);
    } else {
      if(body.length > 0) {
        var newUser = {
          "ID": body.length + 1,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          "password": req.body.password
        };
        client.post('user?' + global.API_KEY, newUser,function(err, respuestaMLab, body) {
          var response = {};
          if(err) {
            response = {"msg" : "Error al crear al usuario."}
            res.status(500);
          }else{
            response = body;
            res.status(201);
          }
          res.send(response);
        });

      } else {
        response = {"msg" : "Ningún elemento 'user'."}
        res.status(404);
      }
    }
  });
}

module.exports.postCreateUserMongo = postCreateUserMongo;

function putModifyUserMongo(req,res){
  var id = req.params.id;
  let userBody = req.body;
  var queryString = 'q={"ID":' + id + '}&';

  client.get('user?'+ queryString + global.API_KEY,
  function(err, respuestaMLab, body) {
    if(err) {
        let response = {"msg" : "Error obteniendo usuario."}
        res.status(500);
    } else {
      let response = body[0];
      //Actualizo campos del usuario
       let updatedUser = {
         "ID" : req.body.ID,
         "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "password" : req.body.password
       };
       //Otra forma simplificada (para muchas propiedades)
       // var updatedUser = {};
       // Object.keys(response).forEach(key => updatedUser[key] = response[key]);
       // Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);
       // PUT a mLab
       console.log(response._id.$oid);
       client.put('user/' + response._id.$oid + '?' + global.API_KEY, updatedUser,
          function(err, respuestaMLab, body){
            var response = {};
            if(err) {
                response = {
                  "msg" : "Error actualizando usuario."
                }
                res.status(500);
            } else {
              if(body.length > 0) {
                response = body;
              } else {
                response = {
                  "msg" : "Usuario actualizado correctamente."
                }
                res.status(200);
              }
            }
            res.send(response);
          });
    }
  });
}
module.exports.putModifyUserMongo = putModifyUserMongo;

function deleteWithPutUserMongo(req,res){
  var id = req.params.id;
  var queryString = 'q={"ID":' + id + '}&';

  client.get('user?'+ queryString + global.API_KEY,
  function(err, respuestaMLab, body) {
    if(err) {
        let response = {"msg" : "Error obteniendo usuario."}
        res.status(500);
    } else {
      let response = body[0];
      let bodyR = [0];
       client.put('user/' + response._id.$oid + '?' + global.API_KEY,bodyR,
          function(err, respuestaMLab, body){
            var response = {};
            if(err) {
                response = {
                  "msg" : "Error al eliminar el usuario."
                }
                res.status(500);
            } else {
              console.log(body);
              console.log(body.length);
              if(body.length > 0) {
                response = body;
              } else {
                response = {
                  "msg" : "Usuario eliminado correctamente."
                }
                res.status(200);
              }
            }
            res.send(response);
          });
    }
  });
}
module.exports.deleteWithPutUserMongo = deleteWithPutUserMongo;

function deleteUserMongo(req,res){
  var id = req.params.id;
  var queryString = 'q={"ID":' + id + '}&';

  client.get('user?'+ queryString + global.API_KEY,
  function(err, respuestaMLab, body) {
    if(err) {
        response = {"msg" : "Error obteniendo usuario."}
        res.status(500);
    } else {
      let response = body[0];
       console.log(response._id.$oid);
       client.delete('user/' + response._id.$oid + '?' + global.API_KEY,
          function(err, respuestaMLab, body){
            var response = {};
            if(err) {
                response = {
                  "msg" : "Error actualizando usuario."
                }
                res.status(500);
            } else {
              res.status(200);
            }
            res.send(response);
          });
    }
  });
}
module.exports.deleteUserMongo = deleteUserMongo;

function deleteIndiceUserMongo(req,res){
  var id = req.params.id;
  var queryString = 'q={"_id":' + id + '}&';


       client.delete('user/' + id + '?' + global.API_KEY,
          function(err, respuestaMLab, body){
            var response = {};
            if(err) {
                response = {
                  "msg" : "Error actualizando usuario."
                }
                res.status(500);
            } else {
              res.status(200);
            }
            res.send(response);
          });

}
module.exports.deleteIndiceUserMongo = deleteIndiceUserMongo;
