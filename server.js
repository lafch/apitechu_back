require('dotenv').config();
const global        = require('./global.js');
var express         = require('express');
var bodyParser      = require('body-parser');

let user_controller = require('./controllers/users_controller');
let auth_controller = require('./controllers/auth_controller');
let account_controller = require('./controllers/account_controller');
var port            = process.env.PORT || 3000;
var httpClient      = express();
    httpClient.use(bodyParser.json());
    httpClient.listen(port);

httpClient.get(global.URL_BASE+'users/:id', user_controller.getSearchUserParameter);
httpClient.get(global.URL_BASE+'users', user_controller.getSearchUserQueryString);
httpClient.post(global.URL_BASE+'users',user_controller.postAddUser);
httpClient.put(global.URL_BASE + 'users/:id',user_controller.putModifyUser);
httpClient.delete(global.URL_BASE + 'users/:id',user_controller.deleteUser);

httpClient.post(global.URL_BASE + 'login',auth_controller.postLogin);
httpClient.post(global.URL_BASE + 'loginOut',auth_controller.postLoginOut);

httpClient.get(global.URL_BASE_V2+'users', user_controller.getUsersMongo);
httpClient.get(global.URL_BASE_V2+'users/:id', user_controller.getUserByIdMongo);
httpClient.get(global.URL_BASE_V2+'users/:id/accounts', account_controller.getAccountByIdUserMongo);
httpClient.post(global.URL_BASE_V2+'users',user_controller.postCreateUserMongo);
httpClient.put(global.URL_BASE_V2+'users/:id',user_controller.putModifyUserMongo);
httpClient.delete(global.URL_BASE_V2+'users/:id',user_controller.deleteUserMongo);
httpClient.delete(global.URL_BASE_V3+'users/:id',user_controller.deleteWithPutUserMongo);

httpClient.post(global.URL_BASE_V2 + 'login',auth_controller.postLoginMongo);
httpClient.post(global.URL_BASE_V2 + 'loginShort',auth_controller.postLoginShortMongo);
httpClient.post(global.URL_BASE_V2 + 'loginOut',auth_controller.postLoginOutMongo);
